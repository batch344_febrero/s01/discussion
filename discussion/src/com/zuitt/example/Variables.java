package com.zuitt.example;
//A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
//Packages are divided into two categories:
//1. Built - in Packages (Packages from the Java API)
//2. User-defined Packages (create your own packages)

//Package creation in Java follows the "reverse domain name notation" for the naming convention.

public class Variables {
    public static void main (String[] args){
        //Naming Convention

        int age;
        char middleName;

        int x;
        int y = 0;

        x = 1;

        System.out.println("The value of y is " + y + "and the value of x is " + x);

        int wholeNumber = 100;
        System.out.println(wholeNumber);

        long worldPopulation = 654563213546L;
        System.out.println(worldPopulation);

        float piFloat = 3.14159265359f;
        System.out.println("The value of piFloat is " + piFloat);

        double piDouble = 3.14159265359;
        System.out.println("The value of piDouble is " + piDouble);

        char letter = 'a';
        System.out.println(letter);

        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        final  int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        String username = "JSmith";
        System.out.println(username);

        int stringLength = username.length();
        System.out.println(stringLength);

    }
}
